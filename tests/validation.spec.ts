import { describe, expect, it, test } from '@jest/globals';
import emailValidationChain from '../src/models/validationSchemas';
import { validationResult } from 'express-validator';
import valid from './mail-requests/minimum-valid-mail.json';
import invalidFrom from './mail-requests/bad-from-mail.json';

describe('mail request form validation', () => {
  test('minimum valid mail request', async () => {
    for (let validation of emailValidationChain) {
      const result = await validation.run(valid);
    }

    const errors = validationResult(valid).array();
    expect(errors).toHaveLength(0);
  });
});

describe.each([
  { req: invalidFrom[0], n: 1, msg: 'This field must be a valid email' },
  { req: invalidFrom[1], n: 1, msg: 'This field must be a valid email' },
  { req: invalidFrom[2], n: 1, msg: 'Must be an object and contain at least an email address' },
  { req: invalidFrom[3], n: 1, msg: 'Must be an object and contain at least an email address' }
])('testing from field', ({ req, n, msg }) => {
  it(`from: ${JSON.stringify(req.body.from)}`, async () => {
    for (let validation of emailValidationChain) {
      const result = await validation.run(req);
    }
    const errors = validationResult(req).array();
    expect(errors).toHaveLength(n);
    expect(errors[0].msg).toBe(msg);
  });
});
