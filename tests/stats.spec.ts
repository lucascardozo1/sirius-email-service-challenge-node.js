import 'reflect-metadata';
import { describe, expect, it, beforeAll } from '@jest/globals';
import { Mock, mock } from 'ts-jest-mocker';
import { IStatsService, IMailCounter } from '../src/ioc-container/interfaces';
import StatsService from '../src/services/statsService';

describe('Stats Service', () => {
  let statsService: IStatsService;
  let mailCounterMock: Mock<IMailCounter>;

  beforeAll(() => {
    mailCounterMock = mock<IMailCounter>();
    statsService = new StatsService(mailCounterMock);
  });

  it('should return mail registry', async () => {
    //GIVEN
    mailCounterMock.getCounter.mockResolvedValue({
      'lucas.david.car99@gmail.com': 1
    });
    //WHEN
    const registry = await statsService.getStats();
    //THEN
    expect(registry['lucas.david.car99@gmail.com']).toBe(1);
  });
});
