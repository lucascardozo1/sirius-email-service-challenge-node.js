import 'reflect-metadata';
import { Mock, mock } from 'ts-jest-mocker';
import { IMailCounter, IMailSender, IMailSenderFactory, IMailService } from '../src/ioc-container/interfaces';
import MailService from '../src/services/mailsService';
import { MailRequest, MailResponse } from '../src/models/mail';
import { expect, describe, it, beforeAll } from '@jest/globals';

describe('MailService', () => {
  let mailService: IMailService;
  let mailCounterMock: Mock<IMailCounter>;
  let mailSenderFactoryMock: Mock<IMailSenderFactory>;
  let mailSenderMock: Mock<IMailSender>;

  beforeAll(() => {
    mailSenderFactoryMock = mock<IMailSenderFactory>();
    mailCounterMock = mock<IMailCounter>();
    mailSenderMock = mock<IMailSender>();

    mailService = new MailService(mailSenderFactoryMock, mailCounterMock);
  });

  it('should throw error with "Email limit reached" message and 403 status code', async () => {
    //GIVEN
    mailCounterMock.isAllowToSendMail.mockResolvedValue(false);
    let req: MailRequest = {
      from: {
        address: 'a'
      },
      to: [
        {
          address: 'a'
        }
      ],
      contentType: 'text',
      content: 'aaa'
    };
    //WHEN
    //THEN
    try {
      await mailService.sendEmail(req);
    } catch (e: any) {
      if (e.name === 'ApiError') {
        expect(e.message).toBe(`Email limit reached for: ${req.from.address}!`);
        expect(e.statusCode).toBe(403);
      } else {
        fail();
      }
    }
  });

  it('should return 200 status code and mail api name "SendGrid"', async () => {
    //GIVEN
    mailSenderMock.sendSimpleMail.mockImplementation(() => {
      return new Promise<MailResponse>((resolve, reject) => {
        resolve({
          statusCode: 200,
          apiUsed: 'SendGrid'
        });
      });
    });
    mailCounterMock.isAllowToSendMail.mockResolvedValue(true);
    mailCounterMock.incrementCounterFor.mockResolvedValue();
    mailSenderFactoryMock.getNextAvailable.mockImplementation(() => {
      return mailSenderMock;
    });
    let req: MailRequest = {
      from: {
        address: 'a'
      },
      to: [
        {
          address: 'a'
        }
      ],
      contentType: 'text',
      content: 'aaa'
    };
    //WHEN
    let response = await mailService.sendEmail(req);

    expect(response.statusCode).toBe(200);
    expect(response.apiUsed).toBe('SendGrid');
  });

  it('should throw error with "No mail sender available" message and 404 status code', async () => {
    mailCounterMock.isAllowToSendMail.mockResolvedValue(true);

    mailSenderFactoryMock.getNextAvailable.mockImplementation(() => {
      return undefined;
    });

    let req: MailRequest = {
      from: {
        address: 'a'
      },
      to: [
        {
          address: 'a'
        }
      ],
      contentType: 'text',
      content: 'aaa'
    };
    //WHEN
    try {
      await mailService.sendEmail(req);
    } catch (e: any) {
      if (e.name === 'ApiError') {
        expect(e.name).toBe('ApiError');
        expect(e.message).toBe('No mail sender available');
        expect(e.statusCode).toBe(404);
      } else {
        fail();
      }
    }
  });
});
