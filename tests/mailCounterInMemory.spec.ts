import 'reflect-metadata';
import { jest, describe, expect, it } from '@jest/globals';
import MailCounterInMemory from '../src/domain/mailCounterInMemory';

describe('tests MailCounter', () => {
  let mailCounter: MailCounterInMemory;

  it('should increment counter', async () => {
    mailCounter = new MailCounterInMemory();
    //GIVEN
    const user: string = 'example@example.com';
    //WHEN
    await mailCounter.incrementCounterFor(user);
    const counter = await mailCounter.getCounter();
    //THEN
    expect(counter[user]).toBe(1);
  });

  it('should get counter for user', async () => {
    //GIVEN
    const user1: string = 'example1@example.com';
    const user2: string = 'example2@example.com';
    mailCounter = new MailCounterInMemory();
    jest.replaceProperty(mailCounter, '__counter' as any, {
      'example2@example.com': 1
    });

    //WHEN
    const mailsSentUser1 = await mailCounter.getCounterFor(user1);
    const mailsSentUser2 = await mailCounter.getCounterFor(user2);
    //THEN
    expect(mailsSentUser1).toBe(0);
    expect(mailsSentUser2).toBe(1);
  });

  it('should tell if user is allowed to send mail', async () => {
    mailCounter = new MailCounterInMemory();
    //GIVEN
    const user1: string = 'example1@example.com';
    const user2: string = 'example2@example.com';
    const user3: string = 'example3@example.com';

    jest.replaceProperty(mailCounter, '__counter' as any, {
      'example1@example.com': 10,
      'example2@example.com': 1000,
      'example3@example.com': 1001
    });
    //WHEN
    const isAllowedUser1 = await mailCounter.isAllowToSendMail(user1);
    const isAllowedUser2 = await mailCounter.isAllowToSendMail(user2);
    const isAllowedUser3 = await mailCounter.isAllowToSendMail(user3);

    //THEN
    expect(isAllowedUser1).toBe(true);
    expect(isAllowedUser2).toBe(true);
    expect(isAllowedUser3).toBe(false);
  });

  it('should reset counter', async () => {
    mailCounter = new MailCounterInMemory();

    const user1: string = 'example1@example.com';
    jest.replaceProperty(mailCounter, '__counter' as any, {
      'example1@example.com': 999
    });

    await mailCounter.resetCounter();

    expect(await mailCounter.getCounterFor(user1)).toBe(0);
  });
});
