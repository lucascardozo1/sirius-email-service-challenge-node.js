import express from 'express';
import authenticationRouter from './src/routes/authenticationRouter';
import mailRouter from './src/routes/mailsRouter';
import statsRouter from './src/routes/statsRouter';
import { ErrorHandlerController } from './src/controller/errorHandlerController';
import { IMailCounter } from './src/ioc-container/interfaces';
import container from './src/ioc-container/inversifyConfig';
import { TYPES } from './src/ioc-container/types';
import cron from 'node-cron';

const errorHandler = new ErrorHandlerController();
const app = express();

app.use(express.json());

app.use('/auth', authenticationRouter);
app.use('/mails', mailRouter);
app.use('/stats', statsRouter);

app.use(errorHandler.handleAuthenticationError);
app.use(errorHandler.handleAuthorizationError);
app.use(errorHandler.handlePersistenceError);
app.use(errorHandler.handleValidationError);
app.use(errorHandler.handleMailApiError);
app.use(errorHandler.handleApiError);
app.use(errorHandler.handleGenericError);

const mailCounter: IMailCounter = container.get<IMailCounter>(TYPES.IMailCounter);
cron.schedule('0 0 * * *', mailCounter.resetCounter);
export default app;
