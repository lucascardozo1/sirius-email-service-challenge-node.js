import 'reflect-metadata';
import dotenv from 'dotenv';
import app from './app';
import http from 'node:http';
import path from 'node:path';

dotenv.config({ path: path.resolve(__dirname, '../.env') });

const port = process.env.APP_PORT || 3000;
app.set('port', port);
const server = http.createServer(app);
server.listen(port);

console.log(`Server is running in port ${port}!!!`);
