import { NextFunction, Request, Response } from 'express';
import { TypedRequest, TypedResponse } from '../models/utils';
import { User, UserCredentials, UserSignupForm } from '../models/user';
import { MailRegistry, MailRequest, MailResponse } from '../models/mail';

interface IAuthenticationController {
  authenticate: (req: TypedRequest<UserCredentials>, res: Response, next: NextFunction) => Promise<Response>;
}
interface IAuthenticationService {
  checkCredentials: (loginForm: UserCredentials) => Promise<User>;
}

interface IAuthorizationManager {
  validateToken: (req: Request, res: Response, next: NextFunction) => Promise<void>;
  hasUserPermissions: (req: Request, res: Response, next: NextFunction) => Promise<void>;
  hasAdminPermissions: (req: Request, res: Response, next: NextFunction) => Promise<void>;
}

interface IUserController {
  createUser: (req: TypedRequest<UserSignupForm>, res: Response, next: NextFunction) => Promise<Response>;
}

interface IUserService {
  createUser: (form: UserSignupForm) => Promise<User>;
  getUserByEmail: (email: string) => Promise<User>;
}

interface IUserRepository {
  createUser: (form: UserSignupForm) => Promise<User>;
  getUserByEmail: (email: string) => Promise<User>;
  incrementMailCounterByEmail: (email: string) => Promise<User>;
  resetAllEmailCounters: () => Promise<void>;
  getAllUsers: () => Promise<User[]>;
}

interface IJwtService {
  issueToken: (user: User) => Promise<string>;
}

interface IMailController {
  sendMail: (req: TypedRequest<MailRequest>, res: Response) => Promise<Response>;
}

interface IMailCounter {
  incrementCounterFor: (email: string) => Promise<void>;
  getCounterFor: (email: string) => Promise<number>;
  isAllowToSendMail: (email: string) => Promise<boolean>;
  resetCounter: () => Promise<void>;
  getCounter: () => Promise<MailRegistry>;
}

interface IMailService {
  sendEmail: (mailForm: MailRequest) => Promise<MailResponse>;
}

interface IMailSenderFactory {
  getNextAvailable: () => IMailSender | undefined;
}

interface IMailSender {
  sendSimpleMail: (req: MailRequest) => Promise<MailResponse>;
  available: boolean;
}

interface IStatsController {
  getStats: (req: Request, res: Response, next: NextFunction) => Promise<Response>;
}

interface IStatsService {
  getStats: () => Promise<MailRegistry>;
}

export {
  IAuthenticationController,
  IAuthenticationService,
  IAuthorizationManager,
  IUserController,
  IUserService,
  IUserRepository,
  IJwtService,
  IMailController,
  IMailCounter,
  IMailService,
  IMailSender,
  IMailSenderFactory,
  IStatsController,
  IStatsService
};
