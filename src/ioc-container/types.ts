const TYPES = {
  IAuthenticationController: Symbol.for('IAuthenticationController'),
  IAuthenticationService: Symbol.for('IAuthenticationService'),
  IMailService: Symbol.for('IMailService'),
  IUserController: Symbol.for('IUserController'),
  IUserService: Symbol.for('IUserService'),
  IUserRepository: Symbol.for('IUserRepository'),
  IJwtService: Symbol.for('IJwtService'),
  IMailController: Symbol.for('IMailController'),
  IMailCounter: Symbol.for('IMailCounter'),
  IMailSenderFactory: Symbol.for('IMailSenderFactory'),
  IMailSender: Symbol.for('IMailSender'),
  IAuthorizationManager: Symbol.for('IAuthorizationManager'),
  IStatsController: Symbol.for('IStatsController'),
  IStatsService: Symbol.for('IStatsService')
};

export { TYPES };
