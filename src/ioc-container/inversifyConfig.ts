import { Container } from 'inversify';
import AuthenticationController from '../controller/authenticationController';
import MailController from '../controller/mailsController';
import UserController from '../controller/usersController';
import AuthorizationManager from '../middleware/authorizationMiddleware';
import MailCounterInMemory from '../domain/mailCounterInMemory';
import PrismaUserRepository from '../repository/userRepository';
import AuthenticationService from '../services/authenticationService';
import JwtService from '../services/jwtService';
import MailService from '../services/mailsService';
import UserService from '../services/usersService';
import StatsController from '../controller/statsController';
import StatsService from '../services/statsService';

import {
  IAuthenticationController,
  IAuthenticationService,
  IAuthorizationManager,
  IMailController,
  IMailCounter,
  IMailService,
  IUserController,
  IUserRepository,
  IUserService,
  IJwtService,
  IStatsController,
  IStatsService,
  IMailSender,
  IMailSenderFactory
} from './interfaces';

import { TYPES } from './types';
import MailSenderFactory from '../services/mailSenderFactory';
import SendgridMailSender from '../domain/sendgridMailSender';
import MailCounterPrisma from '../domain/mailCounterPrisma';

const container = new Container({ defaultScope: 'Singleton' });

container.bind<IAuthenticationController>(TYPES.IAuthenticationController).to(AuthenticationController);
container.bind<IAuthenticationService>(TYPES.IAuthenticationService).to(AuthenticationService);
container.bind<IAuthorizationManager>(TYPES.IAuthorizationManager).to(AuthorizationManager);

container.bind<IMailController>(TYPES.IMailController).to(MailController);
container.bind<IMailCounter>(TYPES.IMailCounter).to(MailCounterPrisma);
container.bind<IMailService>(TYPES.IMailService).to(MailService);
container.bind<IMailSenderFactory>(TYPES.IMailSenderFactory).to(MailSenderFactory);
container.bind<IMailSender>(TYPES.IMailSender).to(SendgridMailSender);

container.bind<IUserController>(TYPES.IUserController).to(UserController);
container.bind<IUserRepository>(TYPES.IUserRepository).to(PrismaUserRepository);
container.bind<IUserService>(TYPES.IUserService).to(UserService);

container.bind<IJwtService>(TYPES.IJwtService).to(JwtService);

container.bind<IStatsController>(TYPES.IStatsController).to(StatsController);
container.bind<IStatsService>(TYPES.IStatsService).to(StatsService);

export default container;
