import sendGrid from '@sendgrid/mail';
import InternalServerError from '../../models/errors/internalServerError';

const apiKey = process.env.SENDGRID_API_KEY;

if (apiKey !== undefined) {
  sendGrid.setApiKey(apiKey);
} else {
  throw new InternalServerError('SendGrid API Key not set!');
}

export default sendGrid;
