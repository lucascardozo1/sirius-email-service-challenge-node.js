import { injectable } from 'inversify';
import { IMailSender } from '../ioc-container/interfaces';
import { MailRequest, MailResponse } from '../models/mail';
import AbstractMailSender from './abstractMailSender';
import sendGrid from './config/sendgridConfig';
import { MailDataRequired, ClientResponse, ResponseError } from '@sendgrid/mail';
import MailSenderError from '../models/errors/mailSenderError';

@injectable()
export default class SendgridMailSender
  extends AbstractMailSender<MailDataRequired, ClientResponse>
  implements IMailSender
{
  protected sendMail = async (mail: MailDataRequired): Promise<ClientResponse> => {
    let [res] = await sendGrid.send(mail);
    return res;
  };

  protected prepareMessage = (req: MailRequest): MailDataRequired => {
    return {
      from: { email: req.from.address, name: req.from.name },
      to: req.to.map((e) => ({ email: e.address, name: e.name })),
      cc: req.cc?.map((e) => ({ email: e.address, name: e.name })),
      bcc: req.bbc?.map((e) => ({ email: e.address, name: e.name })),
      subject: req.subject,
      content: [{ type: req.contentType, value: req.content }]
    };
  };

  protected prepareResponse = (res: ClientResponse): MailResponse => {
    return {
      statusCode: res.statusCode,
      messages: ['Email sent!'],
      apiUsed: 'SendGrid'
    };
  };

  protected prepareError = (err: any): Error => {
    if (err instanceof ResponseError) {
      const castErr = err as SendgridResponseError;
      const errors: string[] = castErr.body.errors.map((sde) => sde.message);
      return new MailSenderError(errors, castErr.code);
    }

    return err;
  };
}

interface SendgridError {
  field: string | null;
  message: string;
}

interface SendgridResponseError extends ResponseError {
  body: {
    errors: SendgridError[];
    id?: string;
  };
}
