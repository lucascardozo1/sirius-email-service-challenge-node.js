import { inject, injectable } from 'inversify';
import { IMailCounter, IUserRepository } from '../ioc-container/interfaces';
import { MailRegistry } from '../models/mail';
import { TYPES } from '../ioc-container/types';

@injectable()
export default class MailCounterPrisma implements IMailCounter {
  private __userRepository: IUserRepository;

  constructor(@inject(TYPES.IUserRepository) userRepository: IUserRepository) {
    this.__userRepository = userRepository;
  }
  getCounterFor = async (email: string) => {
    const user = await this.__userRepository.getUserByEmail(email);
    return user.mailCounter;
  };

  incrementCounterFor = async (email: string) => {
    await this.__userRepository.incrementMailCounterByEmail(email);
  };

  isAllowToSendMail = async (email: string) => {
    const user = await this.__userRepository.getUserByEmail(email);
    return user.mailCounter <= 1000;
  };

  resetCounter = async () => {
    await this.__userRepository.resetAllEmailCounters();
  };

  getCounter = async () => {
    const users = await this.__userRepository.getAllUsers();
    return users.reduce((registry: MailRegistry, { email, mailCounter }) => {
      registry[email] = mailCounter;
      return registry;
    }, {});
  };
}
