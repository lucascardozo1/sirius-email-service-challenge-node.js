import { injectable } from 'inversify';
import { IMailSender } from '../ioc-container/interfaces';
import { MailRequest, MailResponse } from '../models/mail';

@injectable()
export default abstract class AbstractMailSender<ApiMessage, ApiResponse> implements IMailSender {
  protected __available: boolean = true;

  sendSimpleMail = async (req: MailRequest): Promise<MailResponse> => {
    try {
      let mail = this.prepareMessage(req);
      let response = await this.sendMail(mail);
      return this.prepareResponse(response);
    } catch (error: any) {
      this.__available = false;
      throw this.prepareError(error);
    }
  };

  protected abstract prepareMessage: (req: MailRequest) => ApiMessage;

  protected abstract sendMail: (mail: ApiMessage) => Promise<ApiResponse>;

  protected abstract prepareResponse: (res: ApiResponse) => MailResponse;

  protected abstract prepareError: (err: any) => Error;

  get available(): boolean {
    return this.__available;
  }
}
