import { injectable } from 'inversify';
import { IMailCounter } from '../ioc-container/interfaces';
import { MailRegistry } from '../models/mail';

@injectable()
export default class MailCounterInMemory implements IMailCounter {
  private __counter: MailRegistry = {};

  incrementCounterFor = async (email: string) => {
    this.__counter[email] = (this.__counter[email] || 0) + 1;
  };

  getCounterFor = async (email: string) => {
    return this.__counter[email] ?? 0;
  };

  isAllowToSendMail = async (email: string) => {
    return (this.__counter[email] ?? 0) <= 1000;
  };

  getCounter = async () => {
    return this.__counter;
  };

  resetCounter = async () => {
    const that = this;
    Object.keys(this.__counter).forEach(function (key) {
      delete that.__counter[key];
    });
  };
}
