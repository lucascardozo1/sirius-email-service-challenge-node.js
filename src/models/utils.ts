import { Request, Response } from 'express';

interface TypedResponse<T> extends Response {
  body: T;
}
interface TypedRequest<T> extends Request {
  body: T;
}

export { TypedResponse, TypedRequest };
