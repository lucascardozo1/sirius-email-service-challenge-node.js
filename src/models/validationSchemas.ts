import { body } from 'express-validator';

const emailValidationChain = [
  body('**.address').toLowerCase().trim(),
  body('**.name').optional().trim(),
  body('from').isObject().withMessage('Must be an object and contain at least an email address'),
  body('to').isArray({ min: 1 }).withMessage('Must be and array and contain at least one element'),
  body(['cc', 'bcc']).optional().isArray({ min: 1 }).withMessage('Must be and array and contain at least one element'),
  body('from.address').if(body('from').exists()).isEmail().withMessage('This field must be a valid email'),
  body('to.*').if(body('to').exists()).isObject().withMessage('This element must be and object'),
  body('to.*.address').if(body('to').exists()).isEmail().withMessage('This field must be a valid email'),
  body('cc.*').if(body('cc').exists()).isObject().withMessage('This element must be and object'),
  body('cc.*.address').if(body('cc').exists()).isEmail().withMessage('This field must be a valid email'),
  body('bcc.*').if(body('bcc').exists()).isObject().withMessage('This element must be and object'),
  body('bcc.*.address').if(body('bcc').exists()).isEmail().withMessage('This field must be a valid email'),
  body(['subject', 'content']).exists().withMessage('This field is mandatory. Can be null'),
  body('contentType').trim().isIn(['text', 'html']).withMessage('This field is mandatory and it can be text or html')
];

export default emailValidationChain;
