type Role = 'ADMIN' | 'USER';

interface UserUpdate {
  email: string;
  name?: string;
  role?: Role;
  mailCounter?: number;
  password?: string;
}

interface User extends UserCredentials {
  id: number;
  name: string | null;
  role: Role;
  mailCounter: number;
}
interface UserSignupForm extends UserCredentials {
  name?: string;
}
interface UserCredentials {
  email: string;
  password: string;
}

export { User, UserSignupForm, UserCredentials, UserUpdate };
