import ApiError from './apiError';

export default class InternalServerError extends ApiError {
  constructor(message: string) {
    super(message, 500);
    this.name = InternalServerError.name;
  }
}
