import { ValidationError } from 'express-validator';
import ApiError from './apiError';

export default class InputValidationError extends ApiError {
  errors: ValidationError[];

  constructor(message: string, errors: ValidationError[]) {
    super(message, 400);
    this.errors = errors;
    this.name = InputValidationError.name;
  }
}
