import ApiError from './apiError';

export default class AuthenticationError extends ApiError {
  constructor(message: string, statusCode = 401) {
    super(message, statusCode);
    this.name = AuthenticationError.name;
  }
}
