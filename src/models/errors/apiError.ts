export default class ApiError extends Error {
  statusCode: number;
  name: string;
  constructor(message: string, statusCode: number) {
    super(message);
    this.statusCode = statusCode;
    this.name = ApiError.name;
  }
}
