import ApiError from './apiError';

export default class AuthorizationError extends ApiError {
  constructor(message: string, statusCode = 403) {
    super(message, statusCode);
    this.name = AuthorizationError.name;
  }
}
