import ApiError from './apiError';

export default class PersistenceError extends ApiError {
  code: string;

  constructor(message: string, statusCode: number, code: string) {
    super(message, statusCode);
    this.code = code;
    this.name = PersistenceError.name;
  }
}
