import ApiError from './apiError';

export default class MailSenderError extends ApiError {
  messages: string[];
  statusCode: number;

  constructor(messages: string[], statusCode: number = 400) {
    super('Error occurred in Api', statusCode);
    this.messages = messages;
    this.statusCode = statusCode;
    this.name = MailSenderError.name;
  }
}
