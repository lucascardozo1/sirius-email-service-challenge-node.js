interface MailRegistry {
  [email: string]: number;
}
interface Email {
  address: string;
  name?: string;
}
interface MailRequest {
  from: Email;
  to: Email[];
  cc?: Email[];
  bbc?: Email[];
  subject?: string;
  contentType: 'html' | 'text';
  content: string;
}
interface MailResponse {
  statusCode: number;
  messages?: string[];
  apiUsed: 'SendGrid' | 'Mailgun' | 'Postmark';
}

export { MailRegistry, Email, MailRequest, MailResponse };
