import express from 'express';
import container from '../ioc-container/inversifyConfig';
import { IAuthorizationManager, IStatsController } from '../ioc-container/interfaces';
import { TYPES } from '../ioc-container/types';

const authorization: IAuthorizationManager = container.get<IAuthorizationManager>(TYPES.IAuthorizationManager);
const statsController: IStatsController = container.get<IStatsController>(TYPES.IStatsController);
const router = express.Router();

router.use(authorization.validateToken, authorization.hasAdminPermissions);
router.get('/', statsController.getStats);

export default router;
