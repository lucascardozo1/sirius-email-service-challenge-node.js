import express from 'express';
import { IAuthorizationManager, IMailController } from '../ioc-container/interfaces';
import container from '../ioc-container/inversifyConfig';
import { TYPES } from '../ioc-container/types';
import emailValidationChain from '../models/validationSchemas';

const authorization: IAuthorizationManager = container.get<IAuthorizationManager>(TYPES.IAuthorizationManager);
const mailController: IMailController = container.get<IMailController>(TYPES.IMailController);

const router = express.Router();

router.use(authorization.validateToken, authorization.hasUserPermissions);

router.post('/', emailValidationChain, mailController.sendMail);

export default router;
