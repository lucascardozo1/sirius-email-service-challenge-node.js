import express from 'express';
import { IAuthenticationController, IUserController } from '../ioc-container/interfaces';
import container from '../ioc-container/inversifyConfig';
import { TYPES } from '../ioc-container/types';

const authController: IAuthenticationController = container.get<IAuthenticationController>(
  TYPES.IAuthenticationController
);
const userController: IUserController = container.get<IUserController>(TYPES.IUserController);

const router = express.Router();

router.post('/login', authController.authenticate);

router.post('/register', userController.createUser);

export default router;
