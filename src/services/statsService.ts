import { inject, injectable } from 'inversify';
import { IMailCounter, IStatsService } from '../ioc-container/interfaces';
import { MailRegistry } from '../models/mail';
import MailCounterInMemory from '../domain/mailCounterInMemory';
import { TYPES } from '../ioc-container/types';

@injectable()
export default class StatsService implements IStatsService {
  private __mailCounter: IMailCounter;

  constructor(@inject(TYPES.IMailCounter) mailCounter: IMailCounter) {
    this.__mailCounter = mailCounter;
  }

  getStats = async () => {
    return await this.__mailCounter.getCounter();
  };
}
