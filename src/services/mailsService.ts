import { IMailCounter, IMailSenderFactory, IMailService } from '../ioc-container/interfaces';
import { MailRequest, MailResponse } from '../models/mail';
import { inject, injectable } from 'inversify';
import { TYPES } from '../ioc-container/types';
import ApiError from '../models/errors/apiError';
import retry from 'async-retry';

@injectable()
export default class MailService implements IMailService {
  private __mailSenderFactory: IMailSenderFactory;
  private __mailCounter: IMailCounter;

  constructor(
    @inject(TYPES.IMailSenderFactory) mailSenderFactory: IMailSenderFactory,
    @inject(TYPES.IMailCounter) mailCounter: IMailCounter
  ) {
    this.__mailSenderFactory = mailSenderFactory;
    this.__mailCounter = mailCounter;
  }

  sendEmail = async (mailForm: MailRequest) => {
    let response: MailResponse;

    //explanation: if certain error happens or there are no mail senders available
    //or it has retried too many times, it stops retrying and let exception
    //to be handled further in error handlers
    response = await retry(
      async (bail: Function) => {
        if (!(await this.__mailCounter.isAllowToSendMail(mailForm.from.address))) {
          bail(new ApiError(`Email limit reached for: ${mailForm.from.address}!`, 403));
        }
        const sender = this.__mailSenderFactory.getNextAvailable();
        if (!sender) {
          bail(new ApiError('No mail sender available', 404));
        }

        const response = await sender!.sendSimpleMail(mailForm);
        await this.__mailCounter.incrementCounterFor(mailForm.from.address);
        return response;
      },
      {
        retries: 3,
        maxTimeout: 1500,
        minTimeout: 500
      }
    );

    return response;
  };
}
