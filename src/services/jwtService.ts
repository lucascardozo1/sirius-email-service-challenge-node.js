import { User } from '../models/user';
import jwt from 'jsonwebtoken';
import { IJwtService } from '../ioc-container/interfaces';
import { injectable } from 'inversify';

@injectable()
export default class JwtService implements IJwtService {
  issueToken = async (user: User) => {
    return jwt.sign(
      {
        iat: Math.floor(Date.now() / 1000),
        role: user.role
      },
      process.env.TOKEN_SECRET as string,
      {
        expiresIn: process.env.TOKEN_EXP,
        subject: user.email
      }
    );
  };
}
