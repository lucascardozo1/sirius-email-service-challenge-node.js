import { injectable, multiInject } from 'inversify';
import { IMailSender, IMailSenderFactory } from '../ioc-container/interfaces';
import { TYPES } from '../ioc-container/types';
import MailSenderError from '../models/errors/mailSenderError';
import ApiError from '../models/errors/apiError';

@injectable()
export default class MailSenderFactory implements IMailSenderFactory {
  private __mailSenders: IMailSender[];

  constructor(@multiInject(TYPES.IMailSender) mailSenders: IMailSender[]) {
    this.__mailSenders = mailSenders;
  }

  getNextAvailable = (): IMailSender | undefined => {
    const sender = this.__mailSenders.find((ms) => ms.available);
    return sender;
  };
}
