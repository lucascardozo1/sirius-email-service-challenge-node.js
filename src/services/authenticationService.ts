import bcryptjs from 'bcryptjs';
import { IAuthenticationService, IUserService } from '../ioc-container/interfaces';
import { User, UserCredentials } from '../models/user';
import { injectable, inject } from 'inversify';
import { TYPES } from '../ioc-container/types';
import AuthenticationError from '../models/errors/authenticationError';

@injectable()
export default class AuthenticationService implements IAuthenticationService {
  private __userService: IUserService;

  constructor(@inject(TYPES.IUserService) userService: IUserService) {
    this.__userService = userService;
  }

  checkCredentials = async (loginForm: UserCredentials) => {
    const actualUser = await this.__userService.getUserByEmail(loginForm.email);
    if (!actualUser) throw new AuthenticationError('Bad credentials!');

    let valid = await bcryptjs.compare(loginForm.password, actualUser.password);
    if (!valid) throw new AuthenticationError('Bad credentials!');

    return actualUser;
  };
}
