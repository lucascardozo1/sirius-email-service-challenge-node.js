import { IUserRepository, IUserService } from '../ioc-container/interfaces';
import { injectable, inject } from 'inversify';
import { TYPES } from '../ioc-container/types';
import { User, UserSignupForm } from '../models/user';

@injectable()
export default class UserService implements IUserService {
  private __userRepository: IUserRepository;

  constructor(@inject(TYPES.IUserRepository) userRepository: IUserRepository) {
    this.__userRepository = userRepository;
  }
  createUser = async (form: UserSignupForm) => {
    return this.__userRepository.createUser(form);
  };

  getUserByEmail = async (email: string) => {
    return this.__userRepository.getUserByEmail(email);
  };
}
