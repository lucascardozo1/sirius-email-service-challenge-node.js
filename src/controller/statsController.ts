import { inject, injectable } from 'inversify';
import { TYPES } from '../ioc-container/types';
import { NextFunction, Request, Response } from 'express';
import { IStatsController, IStatsService } from '../ioc-container/interfaces';

@injectable()
export default class StatsController implements IStatsController {
  private __statsService: IStatsService;

  constructor(@inject(TYPES.IStatsService) statsService: IStatsService) {
    this.__statsService = statsService;
  }

  getStats = async (req: Request, res: Response, next: NextFunction) => {
    const stats = await this.__statsService.getStats();
    return res.status(200).json(stats);
  };
}
