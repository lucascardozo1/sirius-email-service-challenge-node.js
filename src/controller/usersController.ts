import { NextFunction, Response } from 'express';
import { inject, injectable } from 'inversify';
import { IUserController, IUserService } from '../ioc-container/interfaces';
import { TYPES } from '../ioc-container/types';
import { TypedRequest } from '../models/utils';
import { UserSignupForm } from '../models/user';

@injectable()
export default class UserController implements IUserController {
  private __userService: IUserService;

  constructor(@inject(TYPES.IUserService) userService: IUserService) {
    this.__userService = userService;
  }

  createUser = async (req: TypedRequest<UserSignupForm>, res: Response, next: NextFunction) => {
    const user = await this.__userService.createUser(req.body);
    return res.status(202).send();
  };
}
