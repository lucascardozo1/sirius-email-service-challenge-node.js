import express from 'express';
import ApiError from '../models/errors/apiError';
import AuthenticationError from '../models/errors/authenticationError';
import InputValidationError from '../models/errors/validationError';
import { FieldValidationError } from 'express-validator';
import PersistenceError from '../models/errors/persistenceError';
import MailSenderError from '../models/errors/mailSenderError';
import AuthorizationError from '../models/errors/authorizationError';

export class ErrorHandlerController {
  handleGenericError = async (err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (err.name === 'Error') {
      return res.status(500).send(err.message);
    }
    next(err);
  };

  handleApiError = async (err: ApiError, req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (err.name === 'ApiError') {
      return res.status(err.statusCode).send(err.message);
    }
    next(err);
  };

  handlePersistenceError = async (
    err: PersistenceError,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    if (err.name === 'PersistenceError') {
      return res.status(err.statusCode).send(err.message);
    }
    next(err);
  };

  handleMailApiError = async (
    err: MailSenderError,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    if (err.name === 'MailSenderError') {
      return res.status(err.statusCode).send(err.message);
    }
    next(err);
  };

  handleAuthorizationError = async (
    err: AuthorizationError,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    if (err.name === 'AuthorizationError') {
      return res.status(err.statusCode).send(err.message);
    }
    next(err);
  };

  handleValidationError = async (
    err: InputValidationError,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    if (err.name === 'InputValidationError') {
      const errors = err.errors as FieldValidationError[];

      const formattedErrors = errors.reduce((formattedErrors: any, error: FieldValidationError) => {
        if (!formattedErrors[error.path]) {
          formattedErrors[error.path] = [];
        }
        formattedErrors[error.path].push(error.msg);
        return formattedErrors;
      }, {});

      return res.status(err.statusCode).json({ errors: formattedErrors });
    }

    next(err);
  };

  handleAuthenticationError = async (
    err: AuthenticationError,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    if (err.name === 'AuthenticationError') {
      res.status(err.statusCode).send(err.message);
    }
    next(err);
  };
}
