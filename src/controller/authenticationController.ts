import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { IAuthenticationController, IAuthenticationService, IJwtService } from '../ioc-container/interfaces';
import { TYPES } from '../ioc-container/types';
import { TypedRequest } from '../models/utils';
import { UserCredentials } from '../models/user';
import JwtService from '../services/jwtService';

@injectable()
export default class AuthenticationController implements IAuthenticationController {
  private __authService: IAuthenticationService;
  private __jwtService: JwtService;

  constructor(
    @inject<IAuthenticationService>(TYPES.IAuthenticationService) authService: IAuthenticationService,
    @inject<IJwtService>(TYPES.IJwtService) jwtService: IJwtService
  ) {
    this.__authService = authService;
    this.__jwtService = jwtService;
  }

  authenticate = async (req: TypedRequest<UserCredentials>, res: Response, next: NextFunction) => {
    const userFound = await this.__authService.checkCredentials(req.body);
    const token = await this.__jwtService.issueToken(userFound);
    return res.status(200).json({ token });
  };
}
