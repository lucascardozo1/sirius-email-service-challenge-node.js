import { Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { IMailController, IMailService } from '../ioc-container/interfaces';
import { TypedRequest } from '../models/utils';
import { MailRequest } from '../models/mail';
import { TYPES } from '../ioc-container/types';
import { validationResult, ValidationError } from 'express-validator';
import InputValidationError from '../models/errors/validationError';

@injectable()
export default class MailController implements IMailController {
  private __mailService: IMailService;

  constructor(@inject(TYPES.IMailService) mailService: IMailService) {
    this.__mailService = mailService;
  }

  sendMail = async (req: TypedRequest<MailRequest>, res: Response) => {
    const result = validationResult(req).array();
    if (!(result.length === 0)) {
      throw new InputValidationError('Bad mail info', result);
    }
    const form = req.body;
    const response = await this.__mailService.sendEmail(form);
    return res.status(200).json(response);
  };
}
