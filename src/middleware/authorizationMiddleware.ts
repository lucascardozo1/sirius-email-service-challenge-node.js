import { NextFunction, Request, Response } from 'express';
import { injectable } from 'inversify';
import jwt from 'jsonwebtoken';
import { IAuthorizationManager } from '../ioc-container/interfaces';
import InternalServerError from '../models/errors/internalServerError';
import AuthenticationError from '../models/errors/authenticationError';
import AuthorizationError from '../models/errors/authorizationError';

@injectable()
export default class AuthorizationManager implements IAuthorizationManager {
  validateToken = async (req: Request, res: Response, next: NextFunction) => {
    const secret = process.env.TOKEN_SECRET;
    if (!secret) throw new InternalServerError('Secret not defined!');

    let authHeader = req.headers.authorization;
    //is auth header set?
    if (!authHeader) throw new AuthenticationError('No auth header!');

    //did it split correctly?
    const token = authHeader!.split(' ')[1];
    if (!token) throw new AuthenticationError('No auth header!');

    const payload = jwt.verify(token, secret);
    //is role set?
    if (typeof payload !== 'string') {
      if (!payload.role) throw new AuthenticationError('Invalid token', 400);
      req.headers.role = payload.role;
    }

    next();
  };

  hasUserPermissions = async (req: Request, res: Response, next: NextFunction) => {
    if (req.headers.role !== 'USER') {
      throw new AuthorizationError('No sufficient permits');
    }
    next();
  };

  hasAdminPermissions = async (req: Request, res: Response, next: NextFunction) => {
    if (req.headers.role !== 'ADMIN') {
      throw new AuthorizationError('No sufficient permits');
    }
    next();
  };
}
