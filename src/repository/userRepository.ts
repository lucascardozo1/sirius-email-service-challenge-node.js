import bcryptjs from 'bcryptjs';
import { injectable } from 'inversify';
import { IUserRepository } from '../ioc-container/interfaces';
import { User, UserSignupForm, UserUpdate } from '../models/user';
import { PrismaClient } from '@prisma/client';

@injectable()
export default class PrismaUserRepository implements IUserRepository {
  private __prisma: PrismaClient;

  constructor() {
    this.__prisma = new PrismaClient();
  }

  createUser = async (form: UserSignupForm): Promise<User> => {
    return this.__prisma.user.create({
      data: {
        email: form.email,
        password: await bcryptjs.hash(form.password, 10),
        name: form.name
      }
    });
  };

  getUserByEmail = async (email: string): Promise<User> => {
    const user = await this.__prisma.user.findUniqueOrThrow({
      where: {
        email: email
      }
    });

    return {
      id: user.id,
      email: user.email,
      password: user.password,
      role: user.role,
      name: user.name,
      mailCounter: user.mailCounter
    };
  };

  incrementMailCounterByEmail = async (email: string): Promise<User> => {
    return this.__prisma.user.update({
      where: {
        email: email
      },
      data: {
        mailCounter: {
          increment: 1
        }
      }
    });
  };

  resetAllEmailCounters = async () => {
    this.__prisma.user.updateMany({
      data: {
        mailCounter: 0
      }
    });
  };

  getAllUsers = async (): Promise<User[]> => {
    return this.__prisma.user.findMany();
  };
}
