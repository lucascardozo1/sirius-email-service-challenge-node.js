import { PrismaClient } from '@prisma/client';
import bcryptjs from 'bcryptjs';
const prisma = new PrismaClient();
async function main() {
  const user = await prisma.user.create({
    data: {
      email: 'lucas@example.example',
      name: 'Lucas',
      password: bcryptjs.hashSync('lol123', 10)
    }
  });
  console.log(user);
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
